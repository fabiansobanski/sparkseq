import scala.util.Properties

name := "sparkseq-core"

organization := "pl.edu.pw.elka"

version := "0.1-SNAPSHOT"

scalaVersion := "2.10.6"

publishTo := Some(Resolver.file("file", new File("/var/www/maven.sparkseq001.cloudapp.net/html/maven")))

val DEFAULT_HADOOP_VERSION = "2.7.2"
lazy val hadoopVersion = Properties.envOrElse("SPARK_HADOOP_VERSION", DEFAULT_HADOOP_VERSION)

ScctPlugin.instrumentSettings

scalacOptions ++= Seq("-deprecation", "-feature")

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "1.6.1" % "provided",
  "org.scalatest" %% "scalatest" % "2.2.6" % "test",
  "org.apache.commons" % "commons-math3" % "3.6",
  "org.apache.hadoop" % "hadoop-client" % hadoopVersion % "provided" excludeAll ExclusionRule(organization = "javax.servlet"),
  "org.seqdoop" % "hadoop-bam" % "7.3.1" excludeAll ExclusionRule(organization = "javax.servlet"),
  "com.github.samtools" % "htsjdk" % "1.141",
  "com.github.nscala-time" %% "nscala-time" % "2.10.0"
)

resolvers ++= Seq(
  "Akka Repository" at "http://repo.akka.io/releases/",
  "Typesafe" at "http://repo.typesafe.com/typesafe/releases",
  "Scala Tools Snapshots" at "http://scala-tools.org/repo-snapshots/",
  "ScalaNLP Maven2" at "http://repo.scalanlp.org/repo",
  "Spray" at "http://repo.spray.cc",
  "Sonatype snapshots" at "http://oss.sonatype.org/content/repositories/snapshots/",
  "Hadoop-BAM" at "http://hadoop-bam.sourceforge.net/maven/"
)

testOptions in Test <+= (target in Test) map {
  t => Tests.Argument(TestFrameworks.ScalaTest, "-u", "%s" format (t / "test-reports"))
}

test in assembly := {}

mergeStrategy in assembly <<= (mergeStrategy in assembly) { (old) =>
  {
    case PathList("org", "apache", "commons", xs @ _*) => MergeStrategy.first
    case PathList("org", "apache", "hadoop", xs @ _*) => MergeStrategy.first
    case x => old(x)
  }
}

artifact in (Compile, assembly) ~= { art =>
  art.copy(`classifier` = Some("assembly"))
}

addArtifact(artifact in (Compile, assembly), assembly)
