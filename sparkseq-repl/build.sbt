name := "sparkseq-repl"

version := "0.1-SNAPSHOT"

organization := "pl.edu.pw.elka"

scalaVersion := "2.10.6"

publishTo := Some(Resolver.file("file", new File("/var/www/maven.sparkseq001.cloudapp.net/maven")) )

ScctPlugin.instrumentSettings

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "1.6.1" % "provided",
  "org.scalatest" %% "scalatest" % "2.2.6" % "test",
  "com.github.nscala-time" %% "nscala-time" % "2.10.0",
  "pl.edu.pw.elka" %% "sparkseq-core" % "0.1-SNAPSHOT"
)

mergeStrategy in assembly <<= (mergeStrategy in assembly) { (old) =>
  {
    case PathList("org", "apache", "commons", xs @ _*) => MergeStrategy.first
    case PathList("org", "apache", "hadoop", xs @ _*) => MergeStrategy.first
    case x => old(x)
  }
}

resolvers ++= Seq(
  "Akka Repository" at "http://repo.akka.io/releases/",
  "Typesafe" at "http://repo.typesafe.com/typesafe/releases",
  "Scala Tools Snapshots" at "http://scala-tools.org/repo-snapshots/",
  "ScalaNLP Maven2" at "http://repo.scalanlp.org/repo",
  "Spray" at "http://repo.spray.cc",
  "Sonatype snapshots" at "http://oss.sonatype.org/content/repositories/snapshots/",
  "Hadoop-BAM" at "http://hadoop-bam.sourceforge.net/maven/",
  "SparkSeq" at "http://sparkseq001.cloudapp.net/maven/"
)

testOptions in Test <+= (target in Test) map {
  t => Tests.Argument(TestFrameworks.ScalaTest, "-u", "%s" format (t / "test-reports"))
}
